let random = Math.floor(Math.random(1) * 3);

console.log(random)

let computador = []
let user = []
let scoreComputador = 0
let scoreUser = 0


const button = document.getElementById('findButton')


button.onclick = function () {
    document.getElementById("id").innerText = ''
    let guardarescolha = document.getElementById("input").value;

    let h1 = document.createElement('h1')
    h1.textContent = 'Resultado: '
    h1.style.textAlign = 'center'
    document.getElementById("id").appendChild(h1)


    userChoice(guardarescolha)
    computerChoice()
    result(computador, user)
    showScore(scoreComputador, scoreUser)

}

function computerChoice() {
    if (random === 0) {
        computador = 'papel'

        //titulo do resultado
        let p = document.createElement('p')
        p.innerText = `O computador escolheu: ${computador}`
        p.style.textAlign = 'center'
        // document.getElementById("id").appendChild(p)


        //uma borda para o resultado
        let border = document.createElement('border')
        border.className = 'bordaComputador'
        border.appendChild(p)

        // document.body.appendChild(border)
        document.getElementById("id").appendChild(border)

    }
    else if (random === 1) {
        computador = 'pedra'

        //titulo do resultado
        let p = document.createElement('p')
        p.innerText = `O computador escolheu: ${computador}`
        p.style.textAlign = 'center'
        // document.getElementById("id").appendChild(p)


        //uma borda para o resultado
        let border = document.createElement('border')
        border.className = 'bordaComputador'
        border.appendChild(p)

        // document.body.appendChild(border)
        document.getElementById("id").appendChild(border)

    }
    else if (random === 2) {
        computador = 'tesoura'

        //titulo do resultado
        let p = document.createElement('p')
        p.innerText = `O computador escolheu: ${computador}`
        p.style.textAlign = 'center'
        // document.getElementById("id").appendChild(p)

        //uma borda para o resultado
        let border = document.createElement('border')
        border.className = 'bordaComputador'
        border.appendChild(p)

        // document.body.appendChild(border)
        document.getElementById("id").appendChild(border)
    }

}


function userChoice(guardarescolha) {

    if (guardarescolha === 'papel') {
        user = 'papel'

        //titulo do resultado
        let p = document.createElement('p')
        p.innerText = `Você escolheu: ${user}`
        p.style.textAlign = 'center'
        // document.getElementById("id").appendChild(p)


        //uma borda para o resultado
        let border = document.createElement('border')
        border.className = 'bordaUsuario'
        border.appendChild(p)

        // document.body.appendChild(border)
        document.getElementById("id").appendChild(border)

    }

    else if (guardarescolha === 'pedra') {
        user = 'pedra'

        //titulo do resultado
        let p = document.createElement('p')
        p.innerText = `Você escolheu: ${user}`
        p.style.textAlign = 'center'
        // document.getElementById("id").appendChild(p)

        //uma borda para o resultado
        let border = document.createElement('border')
        border.className = 'bordaUsuario'
        border.appendChild(p)

        // document.body.appendChild(border)
        document.getElementById("id").appendChild(border)

    }

    else if (guardarescolha === 'tesoura') {
        user = 'tesoura'

        //titulo do resultado
        let p = document.createElement('p')
        p.innerText = `Você escolheu: ${user}`
        p.style.textAlign = 'center'
        // document.getElementById("id").appendChild(p)

        //uma borda para o resultado
        let border = document.createElement('border')
        border.className = 'bordaUsuario'
        border.appendChild(p)

        // document.body.appendChild(border)
        document.getElementById("id").appendChild(border)

    }

}
userChoice()


function result(computador, user) {

    let titulo = document.createElement('h2')
    titulo.className = 'teste'


    if (computador === 'pedra' && user == 'tesoura') {
        //resultado
        titulo.textContent = 'O computador venceu! A pedra amassa o papel.'
        //style
        titulo.style.textAlign = 'center'
        //score
        scoreComputador++
        document.getElementById("id").appendChild(titulo)

    }
    else if (user === 'pedra' && computador === 'tesoura') {
        //resultado
        titulo.textContent = 'Parabéns! Você venceu. A pedra amassa o papel.'
        //style
        titulo.style.textAlign = 'center'
        //score
        scoreUser++
        document.getElementById("id").appendChild(titulo)


    }
    else if (computador === 'tesoura' && user === 'papel') {
        //resultado
        titulo.textContent = 'O computador venceu! A tesoura corta o papel.'
        //style
        titulo.style.textAlign = 'center'
        //score
        scoreComputador++
        document.getElementById("id").appendChild(titulo)


    }
    else if (user === 'tesoura' && computador === 'papel') {
        //resultado
        titulo.textContent = 'Parabéns! Você venceu. A tesoura corta o papel.'
        //style
        titulo.style.textAlign = 'center'
        //score
        scoreUser++
        document.getElementById("id").appendChild(titulo)

    }

    else if (computador === 'papel' && user === 'pedra') {
        //resultado
        titulo.textContent = 'O computador venceu! O papel embrulha a pedra.'
        //style
        titulo.style.textAlign = 'center'
        //score
        scoreComputador++
        document.getElementById("id").appendChild(titulo)

    }
    else if (user === 'papel' && computador === 'pedra') {
        //resultado
        titulo.textContent = 'Parabéns! Você venceu. O papel embrulha a pedra.'
        //style
        titulo.style.textAlign = 'center'
        //score
        scoreUser++
        document.getElementById("id").appendChild(titulo)

    }

    else if (user === 'papel' && computador === 'papel') {
        //resultado
        titulo.textContent = 'Empate! Ninguém venceu.'
        //style
        titulo.style.textAlign = 'center'
        document.getElementById("id").appendChild(titulo)

    }
    else if (user === 'tesoura' && computador === 'tesoura') {
        //resultado
        titulo.textContent = 'Empate! Ninguém venceu.'
        //style
        titulo.style.textAlign = 'center'
        document.getElementById("id").appendChild(titulo)

    }
    else if (user === 'pedra' && computador === 'pedra') {
        //resultado
        titulo.textContent = 'Empate! Ninguém venceu.'
        //style
        titulo.style.textAlign = 'center'
        document.getElementById("id").appendChild(titulo)
    }

    else{
        titulo.textContent = 'Por favor, digite um valor valido.'
        document.getElementById("id").appendChild(titulo)
    }
}


function showScore(scoreComputador, scoreUser) {

    //titulo do resultado
    let p = document.createElement('p')
    p.innerText = `SCORE: Computador: ${scoreComputador} vs Usuário: ${scoreUser}`
    p.style.textAlign = 'center'
    // document.getElementById("id").appendChild(p)

    //uma borda para o resultado
    let border = document.createElement('border')
        border.className = 'bordaUsuario'
        border.appendChild(p)

    document.getElementById("id").appendChild(border)
    // document.getElementById("id").appendChild(p)


    // document.body.appendChild(border)
}

